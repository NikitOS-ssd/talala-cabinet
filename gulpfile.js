const gulp = require('gulp');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');


function css_style(done) {

  gulp.src('./scss/style.scss')
    .pipe( sourcemaps.init() )
    .pipe( sass({
      errorLogToConsole: true,
      outputStyle: 'compressed'
    }) )
    .on( 'error', console.error.bind(console) )
    .pipe( autoprefixer({
      overrideBrowserslist: ['last 2 versions'],
      cascade: false
    }) )
    .pipe( rename({suffix: '.min'}) )
    .pipe( sourcemaps.write('./') )
    .pipe( gulp.dest('./css/') );

  done();
}

function print(done) {
  console.log('Hi!');
  done();
}

function watchSass() {
  gulp.watch("./scss/**/*", css_style);
}


// 1 вариант постановки задачи gulp
// gulp.task(css_style);
// gulp.task(print);
gulp.task('default', gulp.series(print, watchSass))


// 2 вариант постановки задачи gulp
// exports.printHello = printHello;
